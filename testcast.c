#include <stdio.h>

void p44_t2u()
{
short int v = -12345;
unsigned short uv = (unsigned short)v;
printf("v = %d,uv = %u\n",v,uv);
}

void p44_u2t()
{
unsigned u =4294967295u; /*UMax_32*/
int tu = (int)u;
printf("u = %u,tu = %d\n",u,tu);
}


int main()
{
p44_t2u();
p44_u2t();
return 0;
}